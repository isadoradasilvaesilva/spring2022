
<?php
/* This is a countdown timer
* End of semester
*/

$secPerMin = 60;
$secPerHour = 60 * $secPerMin;
$secPerDay = 24 * $secPerHour;
$secPerYear = 365 * $secPerDay;

// Current Time
$now = time();

// End of semester time
$EndOfSemester = mktime(0,0,0,5,21,2022);

// Number of seconds between now and then
$seconds = $EndOfSemester - $now;

$Years = floor($seconds/$secPerYear);
$seconds = $seconds - ($Years * $secPerYear);

$Days = floor($seconds / $secPerDay);
$seconds = $seconds - ($Days * $secPerDay);

$Hours = floor($seconds / $secPerHour);
$seconds = $seconds - ($Hours * $secPerHour);

$Minutes = floor($seconds / $secPerMin);
$seconds = $seconds - ($Minutes * $secPerMin);


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css" />
    <title>Countdown</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>
    <h3>End of Semester Countdown</h3><br />
    <p>Years: <?=$Years ?> | Days: <?=$Days ?> | Hours: <?=$Hours ?> | Minutes: <?=$Minutes ?> | Seconds: <?=$seconds ?></p>
</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>