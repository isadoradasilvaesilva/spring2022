<?php

$db_dsn = 'mysql:host=localhost;dbname=phpclass';
$db_username = "dbuser";
$db_password = "dbdev123";
$db_options = array (
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
);

try{

    $db = new PDO($db_dsn,$db_username,$db_password,$db_options);
    $sql = $db->prepare("SELECT * FROM phpclass.costumerList;");//taking order
    $sql->execute();
    $rows = $sql->fetchAll();
    //echo "<pre>";
    //print_r($rows);
    //echo "</pre>";

} catch(PDOException $e){
    echo $e->getMessage();
    exit;
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />
    <title>Costumer list</title>
</head>

<body>


<main>

    <h3 id="h3costumers">List of Costumers</h3>

    <?php if(isset($_GET['success']) && $_GET['success'] == 1 ){?>
        <p class="success">Costumer Updated/Added Successfully!</p>
    <?php } ?>

    <?php if(isset($_GET['delete']) && $_GET['delete'] == 1){?>
        <p class="success">Costumer Deleted Successfully!</p>
    <?php } ?>

    <table border="2" id="list">
        <tr>
            <th>ID Costumer</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Address</th>
            <th>City</th>
            <th>State</th>
            <th>Zip</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Password</th>
        </tr>

        <?php foreach($rows as $client): ?>
            <tr>
                <td><a href="updatedelete.php?id=<?=$client['ID_Costumer']?>"><?=$client['ID_Costumer']?></a></td>
                <td><a href="updatedelete.php?id=<?=$client['ID_Costumer']?>"><?=$client['FirstName']?></a></td>
                <td><a href="updatedelete.php?id=<?=$client['ID_Costumer']?>"><?=$client['LastName']?></a></td>
                <td><?=$client['Address']?></td>
                <td><?=$client['City']?></td>
                <td><?=$client['State']?></td>
                <td><?=$client['Zip']?></td>
                <td><?=$client['Phone']?></td>
                <td><?=$client['Email']?></td>
                <td>Secret</td>

            </tr>
        <?php endforeach; ?>
    </table>
    <br/>

    <a href="/costumerlist/submit.php">Create an Account</a>

</main>

</body>

</html>
