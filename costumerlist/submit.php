<?php

$firstName = $_POST['FirstName'];
$lastName = $_POST['LastName'];
$phone = $_POST['Phone'];
$email = $_POST['Email'];
$address = $_POST['Address'];
$city = $_POST['City'];
$zip = $_POST['Zip'];
$state = $_POST['State'];
$pwrd = $_POST['Pwrd'];
$pwrdV = $_POST['PwrdV'];

$required = array($firstName, $lastName, $phone, $email, $address, $city, $zip, $state, $pwrd, $pwrdV);

for ($i=0;$i<=10;$i++){
    if (isset($required[$i]) && empty($required[$i])) {
        $success = false;
        break;
    } else if (isset($required[$i]) && !empty($required[$i])){
        $success = true;
    }
}

if ($success === true){
    if($pwrd === $pwrdV) {
        $db_dsn = 'mysql:host=localhost;dbname=phpclass';
        $db_username = "dbuser";
        $db_password = "dbdev123";
        $db_options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

        try {

            $costumer_key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X',
                mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
                mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535),
                mt_rand(0, 65535), mt_rand(0, 65535));

            $db = new PDO ($db_dsn, $db_username, $db_password, $db_options);
            $sql = $db->prepare("
                    INSERT INTO
                        phpclass.costumerList(FirstName, LastName, Address, City, State, Zip, Phone, Email, Password, Costumer_key)
                        VALUE(:FirstName, :LastName, :Address, :City, :State, :Zip, :Phone, :Email, :Password, :Costumer_key)  ");
            $sql->bindValue(':FirstName', $firstName);
            $sql->bindValue(':LastName', $lastName);
            $sql->bindValue(':Phone', $phone);
            $sql->bindValue(':Email', $email);
            $sql->bindValue(':Address', $address);
            $sql->bindValue(':City', $city);
            $sql->bindValue(':Zip', $zip);
            $sql->bindValue(':State', $state);
            $sql->bindValue(':Password', md5($pwrd.$costumer_key));
            $sql->bindValue(':Costumer_key', $costumer_key);
            $sql->execute();

            header("Location:costumerlist.php");

        } catch (PDOException $e) {
            echo "DB ERROR: " . $e->getMessage();
            exit;
        }
    } else{
        $error = "Password and Password Verification do not match";
    }
}else if ($success === false){
    $error = "There is some information missing. Fill all the fields and try again.";
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />
    <title>Costumer Sign-Up</title>
</head>

<body>


<main>

    <form method="post">

        <h1 id="h3create">Create Account</h1>

        <?php if(isset($error)){?>
            <p class="error"> <?= $error; ?></p>
        <?php } ?>

        <table class="info">
            <tr><th colspan="2"> Costumer </th></tr>
            <tr><th>First Name: <input type="text" name="FirstName" id="FirstName" value="<?=$firstName?>" /></th></tr>
            <tr><th>Last Name: <input type="text" name="LastName" id="LastName" value="<?=$lastName?>" /></th></tr>
            <tr><th>Phone Number: <input type="tel" name="Phone" id="Phone" placeholder="(###)###-####" value="<?=$phone?>" /></th></tr>
            <tr><th>E-mail: <input type="email" name="Email" id="Email" placeholder="example@test.com" value="<?=$email?>" /></th></tr>
        </table>

        <table class="info">
            <tr><th colspan="2"> Address </th></tr>
            <tr><th>Address: <input type="text" name="Address" id="Address" value="<?=$address?>" /></th></tr>
            <tr><th>City: <input type="text" name="City" id="City" value="<?=$city?>" /></th></tr>
            <tr><th>ZIP Code: <input type="text" minlength="5" maxlength="5" size="5" name="Zip" id="Zip" value="<?=$zip?>" /></th></tr>
            <tr><th>State: <select name="State" id="State" value="<?=$state?>">
                        <option value="Alabama">AL</option>
                        <option value="Alaska">AK</option>
                        <option value="Arizona">AZ</option>
                        <option value="Arkansas">AR</option>
                        <option value="California">CA</option>
                        <option value="Colorado">CO</option>
                        <option value="Connecticut">CT</option>
                        <option value="Delaware">DE</option>
                        <option value="Florida">FL</option>
                        <option value="Georgia">GA</option>
                        <option value="Hawaii">HI</option>
                        <option value="Idaho">IA</option>
                        <option value="Illinois">IL</option>
                        <option value="Indiana">IN</option>
                        <option value="Iowa">IA</option>
                        <option value="Kansas">KS</option>
                        <option value="Kentucky">KY</option>
                        <option value="Louisiana">LA</option>
                        <option value="Maine">MA</option>
                        <option value="Maryland">MD</option>
                        <option value="Massachusetts">MA</option>
                        <option value="Michigan">MI</option>
                        <option value="Minnesota">MN</option>
                        <option value="Mississippi">MS</option>
                        <option value="Missouri">MO</option>
                        <option value="Montana">MT</option>
                        <option value="Nebraska">NE</option>
                        <option value="Nevada">NV</option>
                        <option value="New Hampshire">NH</option>
                        <option value="New Jersey">NJ</option>
                        <option value="New Mexico">NM</option>
                        <option value="New York">NY</option>
                        <option value="North Carolina">NC</option>
                        <option value="North Dakota">ND</option>
                        <option value="Ohio">OH</option>
                        <option value="Oklahoma">OK</option>
                        <option value="Oregon">OR</option>
                        <option value="Pennsylvania">PA</option>
                        <option value="Rhode Island">RI</option>
                        <option value="South Carolina">SC</option>
                        <option value="South Dakota">SD</option>
                        <option value="Tennessee">TN</option>
                        <option value="Texas">TX</option>
                        <option value="Utah">UT</option>
                        <option value="Vermont">VT</option>
                        <option value="Virginia">VA</option>
                        <option value="Washington">WA</option>
                        <option value="West Virginia">WV</option>
                        <option value="Wisconsin">WI</option>
                        <option value="Wyoming">WY</option></select></tr>
        </table>

        <table class="info">
            <tr><th colspan="2"> Security </th></tr>
            <tr><th>Password: <input type="password" name="Pwrd" id="Pwrd" /></th></tr>
            <tr><th>Password Verification: <input type="password" name="PwrdV" placeholder="Re-enter Password" id="PwrdV" /></th></tr>
        </table>

        <table id="submit">
            <tr height="100px">
                <td colspan="2"><input type="submit" name="submit_costumer" id="submit_costumer" value="Create Account"/></td>
                <td colspan="2"><input type="reset" name="Reset" id="Reset" value="Reset"/></td>
            </tr>
        </table>

</main>

</body>

</html>