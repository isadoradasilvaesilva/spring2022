<?php include '../includes/db_con.php' ?>

<?php

$db_dsn = 'mysql:host=localhost;dbname=phpclass';
$db_username = "dbuser";
$db_password = "dbdev123";
$db_options = array (
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
);

try{

        $db = new PDO($db_dsn,$db_username,$db_password,$db_options);
        $sql = $db->prepare("SELECT * FROM phpclass.movie_list;");//taking order
        $sql->execute();//baking order
        $rows = $sql->fetchAll();//delivery
        //echo "<pre>";
        //print_r($rows);// eat it
        //echo "</pre>";



    } catch(PDOException $e){
        echo $e->getMessage();
        exit;
    }

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />
    <title>My Movie List</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>
    <h3>My Movie List</h3>

    <?php if(isset($_GET['success']) && $_GET['success'] == 1 ){?>
    <p class="success">Movie Updated/Added Successfully!</p>
    <?php } ?>

    <?php if(isset($_GET['delete']) && $_GET['delete'] == 1){?>
    <p class="success">Movie Deleted Successfully!</p>
    <?php } ?>



    <table border="1" width="80%">
        <tr>
            <th>ID</th>
            <th>Title</th>
            <th>Rating</th>
        </tr>

        <?php foreach($rows as $movie): ?>
        <tr>
            <td><?=$movie['movieID']?></td>
            <td><a href="update.php?id=<?=$movie['movieID']?>"><?=$movie['movieTitle']?></a></td>
            <td><?=$movie['movieRating']?></td>
        </tr>
        <?php endforeach; ?>
    </table>

    <table>
        <p>
            <a href="app.php">Add New Movie</a>
        </p>
    </table>

</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>