<?php

$db_dsn = 'mysql:host=localhost;dbname=phpclass';
$db_username = "dbuser";
$db_password = "dbdev123";
$db_options = array (
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

if (isset($_GET['id']) && !empty($_GET['id']) && FILTER_VALIDATE_INT){

    $id = $_GET['id'];
    // echo $id;exit;
    try{
        $db = new PDO ($db_dsn, $db_username, $db_password,$db_options);
        $sql = $db->prepare("
        DELETE FROM phpclass.movie_list
        WHERE movieID = :id");
        $sql->bindValue(":id", $id);
        $sql->execute();
.
        header("Location: movielist.php?delete=1");

    }catch(PDOException $e){
        echo $e->getMessage();
        exit;
    }

}else{
    header("Location: movielist.php?delete=0");
    exit;
}