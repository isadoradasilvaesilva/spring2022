<?php
    if (isset($_POST['movie_name']) && !empty($_POST['movie_name'])
    && isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])){
        // echo "<pre>"; print_r($_POST); echo "</pre>";
        $title = $_POST["movie_name"];
        $rating = $_POST["movie_rating"];

        // -- DB Stuff

        $db_dsn = 'mysql:host=localhost;dbname=phpclass';
        $db_username = "dbuser";
        $db_password = "dbdev123";
        $db_options = array (
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        );

        try{
            $db = new PDO ($db_dsn, $db_username, $db_password,$db_options);
            $sql = $db->prepare("
            INSERT INTO
                phpclass.movie_list(movieTitle, movieRating)
                VALUE(:Title, :Rating)
            ");
            $sql->bindValue(':Title',$title);
            $sql->bindValue(':Rating',$rating);
            $sql->execute();

            // exit('DB Success');

        }catch (PDOException $e){
            echo "DB ERROR: " . $e->getMessage();
            exit;
        }

        header("Location:movielist.php?success=1");

    } else if (isset($_POST['movie_name']) && empty($_POST['movie_name'])){
        $error = "Please ensure you have added both title and rating before submitting the movie.";

    } else if (isset($_POST['movie_rating']) && empty($_POST['movie_rating'])){
        $error = "Please ensure you have added both title and rating before submitting the movie.";

    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />
    <title>Add Movie</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>
    <form method="post">



    <?php if(isset($error)){?>
        <p class="error"> <?= $error; ?></p>
    <?php } ?>


    <table width="50%">
        <tr height="100px">
            <th colspan="2"><h2>Add New Movie</h2></th>
        </tr>
        <tr height="50px">
            <th>Movie Name</th>
            <td><input type="text" name="movie_name" id="movie_name"/></td>
        </tr>
        <tr height="50px">
            <th>Movie Rating</th>
            <td><input type="text" name="movie_rating" id="movie_rating"/></td>

        </tr>
        <tr height="100px">
            <td colspan="2"><input type="submit" name="submit_movie" id="submit_movie" value="Add Movie"/></td>
        </tr>
    </table>

    </form>

</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>