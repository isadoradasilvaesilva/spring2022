<?php

    if(isset($_GET['id']) && !empty($_GET['id']) && filter_var(($_GET['id']), FILTER_VALIDATE_INT)){

        $id = $_GET['id'];

        try{
            $db_dsn = 'mysql:host=localhost;dbname=phpclass';
            $db_username = "dbuser";
            $db_password = "dbdev123";
            $db_options = array (
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

            $db = new PDO($db_dsn, $db_username,$db_password,$db_options);
            $sql = $db->prepare(" SELECT * FROM phpclass.movie_list WHERE movieID = :id");
            $sql->bindValue(":id", $id);
            $sql->execute();
            $row = $sql->fetch();

            $title = $row["movieTitle"];
            $rating = $row["movieRating"];

        }catch(PDOException $e){
            $error = $e->getMessage();
            echo "Error: $error";
        }

    } else{
        header("Location: movielist.php");
    }

    if (isset($_POST['movie_name']) && !empty($_POST['movie_name'])
    && isset($_POST['movie_rating']) && !empty($_POST['movie_rating'])){
        // echo "<pre>"; print_r($_POST); echo "</pre>";
        $title = $_POST["movie_name"];
        $rating = $_POST["movie_rating"];

        try{
            $db = new PDO ($db_dsn, $db_username, $db_password,$db_options);
            $sql = $db->prepare("
            UPDATE
                phpclass.movie_list set
                movieTitle = :Title,
                movieRating = :Rating
            WHERE
                movieID = :id
            ");
            $sql->bindValue(':Title',$title);
            $sql->bindValue(':Rating',$rating);
            $sql->bindValue(':id',$id);
            $sql->execute();

            header("Location:movielist.php?");

            // exit('DB Success');

        }catch (PDOException $e){
            echo "DB ERROR: " . $e->getMessage();
            exit;
        }



    } else if (isset($_POST['movie_name']) && empty($_POST['movie_name'])){
        $error = "Please ensure you have added both title and rating before submitting the movie.";

    } else if (isset($_POST['movie_rating']) && empty($_POST['movie_rating'])){
        $error = "Please ensure you have added both title and rating before submitting the movie.";

    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />

    <script type="text/javascript">
        function DeleteMovie(moviename, id){
            // alert(moviename + " : " + id);
            if(confirm("Do you really want to delete " + moviename + "?")){
                // alert(moviename + " : " + id);
                document.location.href = "delete.php?id="+id;
            }
        }
    </script>

    <title>Update Movie</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>
    <form method="post">
        <input type="hidden" name="movie_id" id="movie_id" value="<?= $id ?>">


    <?php if(isset($error)){?>
        <p class="error"> <?= $error; ?></p>
    <?php } ?>


    <table width="50%">
        <tr height="100px">
            <th colspan="2"><h2>Update Movie</h2></th>
        </tr>
        <tr height="50px">
            <th>Movie Name</th>
            <td><input type="text" name="movie_name" id="movie_name" value="<?=$title?>" /></td>
        </tr>
        <tr height="50px">
            <th>Movie Rating</th>
            <td><input type="text" name="movie_rating" id="movie_rating" value="<?=$rating?>" /></td>

        </tr>
        <tr height="100px">
            <td colspan="2"><input type="submit" name="submit_movie" id="submit_movie" value="Update Movie"/></td>
            <td colspan="2"><input type="button" name="delete_movie" id="delete_movie" value="Delete Movie"
            onClick="DeleteMovie('<?=$title?>','<?=$id?>')"/></td>

        </tr>
    </table>

    </form>

</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>