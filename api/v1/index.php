<?php

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/* put = update
 * post = insert
 * get = select
 * delete = delete
 */

$app = new \Slim\Slim();

$app->get('/getHello','getHello');
$app->get('/showMember/:MemberName','showMember');
$app->post('/addMember/:MemberName','addMember');
$app->post('/addJson','addJson');
$app->delete('/delUser/:userID','delUser');
$app->run();

function getHello(){
    echo 'Hello World';
}

function showMember($MemberName){
    echo "Hello $MemberName";
}

function addMember($MemberName){
    echo "Hello $MemberName";
}

function addJson(){
    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);

    echo $post_json['address'];
}

function delUser($userID){
    echo "User: $userID was DELETED!";
}