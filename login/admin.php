<?php
session_start();

if($_SESSION['ROLE' !== 1]){
    header("location: index.php");
}


// data validation

if(isset($_POST['user_submit'])){
    if(isset($_POST['txt_name']) && !empty($_POST['txt_name'])){
    $fullname = $_POST['txt_name'];
    }else{
        $error[] = "Name is required.";
    }

    if(isset($_POST['txt_email']) && !empty($_POST['txt_email'])){
        $email = $_POST['txt_email'];
    }else{
        $error[] = "Email is required.";
    }

    if(isset($_POST['txt_role']) && !empty($_POST['txt_role'])){
        $role = $_POST['txt_role'];
    }else{
        $error[] = "Role is required.";
    }

    if(isset($_POST['txt_password']) && !empty($_POST['txt_password'])){
        $_password = $_POST['txt_password'];
    }else{
        $error[] = "Password is required.";
    }

    if($_password != $_POST['txt_verify_password']){
        $error[] = "Password fields must match.";
    }else{
        $password_verify = $_POST['txt_verify_password'];
    }

    $db_dsn = 'mysql:host=localhost;dbname=phpclass';
    $db_username = "dbuser";
    $db_password = "dbdev123";
    $db_options = array (
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

    try{

        $db = new PDO($db_dsn, $db_username, $db_password,$db_options);
        $sql = $db->prepare(" SELECT member_id FROM phpclass.member_login WHERE email = :Email");

        $sql->bindValue(':Email', $email);
        $sql->execute();
        $row = $sql->fetch();

        if($row !== false){
            $error[] = $email." already exists as an account!";
        }

    }catch (PDOException $e){
        echo $e->getMessage();
        exit;
    }


    if(empty($error)){

        $db_dsn = 'mysql:host=localhost;dbname=phpclass';
        $db_username = "dbuser";
        $db_password = "dbdev123";
        $db_options = array (
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

        try{

            $member_key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X',
                mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
                mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535),
                mt_rand(0, 65535), mt_rand(0, 65535));

            // echo strlen($member_key); exit;

            $db = new PDO($db_dsn, $db_username, $db_password,$db_options);
            $sql = $db->prepare(" INSERT INTO phpclass.member_login(name, email, role_id, password, member_key) 
            VALUE (:Name, :Email, :Roleid, :Password, :Key)");

            $sql->bindValue(':Name',$fullname);
            $sql->bindValue(':Email',$email);
            $sql->bindValue(':Roleid',$role);
            $sql->bindValue(':Password',md5($_password.$member_key));
            $sql->bindValue(':Key', $member_key);

            $sql->execute();

        }catch (PDOException $e){
            echo $e->getMessage();
            exit;
        }

        $error[] = "User Registered!";
        unset($fullname,$email,$role, $_password, $password_verify);
    }
}

    $db_dsn = 'mysql:host=localhost;dbname=phpclass';
    $db_username = "dbuser";
    $db_password = "dbdev123";
    $db_options = array (
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

    try{

        $db = new PDO($db_dsn,$db_username,$db_password,$db_options);
        $sql = $db->prepare("SELECT role_id, role_value FROM phpclass.member_roles;");//taking order
        $sql->execute();
        $rows = $sql->fetchAll();

    } catch(PDOException $e){
        echo $e->getMessage();
        exit;
    }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />
    <title>Admin Login</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>

    <h2>Admin Login</h2>

    <form method="post">

        <?php if(isset($error) && !empty($error)){?>
            <?php foreach($error as $e){?>
                 <p class="error"><?= $e; ?></p>
        <?php }} ?>


        <table border="1" width="50%">

            <tr height="100px">
                <th colspan="2"><h3>User Submiting</h3></th>
            </tr>

            <tr height="50px">
                <th>Full Name</th>
                <td><input type="text" name="txt_name" id="txt_name" value="<?=$fullname?>" /></td>
            </tr>

            <tr height="50px">
                <th>E-mail</th>
                <td><input type="text" name="txt_email" id="txt_email" value="<?=$email?>" /></td>
            </tr>

            <tr height="50px">
                <th>Role</th>
                <td>
                    <select id="txt_role" name="txt_role">
                        <?php foreach($rows as $role):?>
                        <option value="<?=$role['role_id']?>"><?=$role['role_value']?></option>
                        <?php endforeach; ?>
                    </select>

                </td>

            </tr>

            <tr height="50px">
                <th>Password</th>
                <td><input type="password" name="txt_password" id="txt_password" value="<?=$_password?>" /></td>

            </tr>

            <tr height="50px">
                <th>Verify Password</th>
                <td><input type="password" name="txt_verify_password" id="txt_verify_password" value="<?=$password_verify?>" /></td>

            </tr>

            <tr height="100px">
                <td colspan="2"><input type="submit" name="user_submit" id="user_submit" value="Create User"/></td>

            </tr>
        </table>

    </form>

</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>