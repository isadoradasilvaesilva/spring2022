<?php

session_start();

if(isset($_SESSION['UID'])){
    unset($_SESSION['UID']);
    $error = "Member user logged out";
}

if (isset($_POST['txt_email']) && !empty($_POST['txt_email'])
    && isset($_POST['txt_password']) && !empty($_POST['txt_password'])) {

    $user_email = strtolower($_POST["txt_email"]);
    $user_password = $_POST["txt_password"];

    $db_dsn = 'mysql:host=localhost;dbname=phpclass';
    $db_username = "dbuser";
    $db_password = "dbdev123";
    $db_options = array (
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

    $db = new PDO($db_dsn, $db_username, $db_password, $db_options);

    $sql = $db->prepare("
        SELECT
            password, role_id, member_key
        FROM
            phpclass.member_login
        WHERE
            email = :Email
    ");

    $sql->bindValue(':Email', $user_email);
    $sql->execute();

    $row = $sql->fetch();

    if($row !== false){
        $hashed_password = md5($user_password.$row['member_key']);

        if($hashed_password == $row['password'] && $row['role_id'] == 1){
            $_SESSION['UID'] = $row['member_key'];
            $_SESSION['ROLE'] = $row['role_id'];
            header("Location: Member.php");

        } else if ($hashed_password == $row['password'] && $row['role_id'] !== 1){

            $_SESSION['UID'] = $row['member_key'];
            $_SESSION['ROLE'] = $row['role_id'];
            header("Location: member.php");

        }else{
            $error = 'Email ou password invalid';
        }

    }else{
        // invalid e-mail address
        $error = 'Email ou password invalid';
    }
}

/*// login stuff
if($user_email == 'admin' && $user_password == 'p@ss'){
    $_SESSION['UID'] = 1;

header("location: Member.php");

} else if($user_email == 'member' && $user_password == 'p@ss'){
header("location: member.php");

}else {
    $error = "Wrong username or password.";
}

} else if (isset($_POST) && !empty($_POST)){
$error = "Wrong username or password.";
}*/

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />

    <title>Login Form</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>
    <form method="post">

        <?php if(isset($error)){?>
            <p class="error"> <?= $error; ?></p>
        <?php } ?>


        <table border="1" width="50%">
            <tr height="100px">
                <th colspan="2"><h2>Login</h2></th>
            </tr>
            <tr height="50px">
                <th>E-mail</th>
                <td><input type="text" name="txt_email" id="txt_email" value="<?=$user_email?>" /></td>
            </tr>
            <tr height="50px">
                <th>Password</th>
                <td><input type="password" name="txt_password" id="txt_password" value="<?=$user_password?>" /></td>

            </tr>
            <tr height="100px">
                <td colspan="2"><input type="submit" name="login_submit" id="login_submit" value="Login"/></td>

            </tr>
        </table>

    </form>

</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>