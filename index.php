<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="CSS/base.css?ver=2.0" />
    <title>Isadora's Homepage</title>
</head>

<body>

<header><?php include 'includes/header.php' ?></header>

<nav><?php include 'includes/nav.php' ?></nav>

<main>
    <img src="img/meandcamera.png" alt="Isadora Holding a Camera"/>
    <p>My name is Isadora and I am an Exchange student from Brazil.<br /></p>
        <p>I’m currently doing Computer Sciences, focusing on Software Development, but I still have my IT Management course in Brazil, which I plan to complete.<br/>
        The programming languages that I had already contact with are Python, C++, C# and PHP.<br/>
        My interests are painting, drawing, writing, learn new languages, play videogames and have a quality time with my friends.</p>
</main>

<footer> <?php include 'includes/footer.php' ?></footer>

</body>

</html>