<?php

$player = [mt_rand(1,6), mt_rand(1,6)];
$computer = [mt_rand(1,6), mt_rand(1,6), mt_rand(1,6)];

$p_result = $player[0] + $player[1];
$c_result = $computer[0] + $computer[1] + $computer[2];

$winner = '';
// echo "<img src='img/dice_$player[0].png'> <br>";

if($p_result > $c_result){
    $winner = "You Win!";
}else if($p_result < $c_result){
    $winner = "Computer Wins!";
}else{
    $winner = "It is a tie.";
}
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" />
    <title>Play Dice</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>
    <h3>Your score: <?=$p_result?></h3><br>
    <img src="img/dice_<?=$player[0]?>.png" alt="1 Dice Player" width="100px" height="100px">
    <img src="img/dice_<?=$player[1]?>.png" alt="2 Dice Player" width="100px" height="100px">

    <h3>Computer score: <?=$c_result?></h3><br>
    <img src="img/dice_<?=$computer[0]?>.png" alt="1 Dice PC" width="100px" height="100px">
    <img src="img/dice_<?=$computer[1]?>.png" alt="2 Dice PC" width="100px" height="100px">
    <img src="img/dice_<?=$computer[2]?>.png" alt="2 Dice PC" width="100px" height="100px">
    <br>

    <h2>Result: <?=$winner?> </h2>
</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>