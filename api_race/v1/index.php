<?php

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

/* put = update
 * post = insert
 * get = select
 * delete = delete
 */

$app = new \Slim\Slim();

$app->get('/get_races','get_races'); // select
$app->get('/get_runners/:race_id','get_runners'); // select
$app->post('/add_runner/:runner_id','addRunner'); // insert
$app->put('/update_runner/:runner_id','uptRunner'); // update
$app->delete('/delete_runner/:runner_id','delRunner'); // delete

$app->run();

function get_races(){

    $db_dsn = 'mysql:host=localhost;dbname=phpclass';
    $db_username = "dbuser";
    $db_password = "dbdev123";
    $db_options = array (
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

    try {

        $db = new PDO($db_dsn, $db_username, $db_password,$db_options);
        $sql = $db->prepare ("SELECT * FROM phpclass.races");
        $sql->execute();
        $result['races'] = $sql->fetchAll();

        echo json_encode($result);


    } catch(PDOException $e){
        $error = $e->getMessage();
        $errors['error'] = $error;
        echo json_encode($errors);
    }

}

function get_runners ($race_id){

    $db_dsn = 'mysql:host=localhost;dbname=phpclass';
    $db_username = "dbuser";
    $db_password = "dbdev123";
    $db_options = array (
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

    try {
        $db = new PDO($db_dsn, $db_username, $db_password,$db_options);
        $sql = $db->prepare ("
    SELECT
	    member_login.`name`, 
	    member_login.email
    FROM
	    member_race
	INNER JOIN
	    member_login ON member_race.member_id = member_login.member_id
    WHERE
	    member_race.race_id = :race_id");
        $sql->bindValue(':race_id', $race_id);
        $sql->execute();
        $result['runners'] = $sql->fetchAll();

        echo json_encode($result);


    } catch(PDOException $e){
        $error = $e->getMessage();
        $errors['error'] = $error;
        echo json_encode($errors);
    }

}

function addRunner($runner_id)
{

    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);

    $member_id = $post_json['member_id'];
    $race_id = $post_json['race_id'];
    $member_key = $post_json['member_key'];

    //echo "$member_id => $race_id => $member_key";
    $db_dsn = 'mysql:host=localhost;dbname=phpclass';
    $db_username = "dbuser";
    $db_password = "dbdev123";
    $db_options = array(
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

    try {

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("
	SELECT
	    member_race.race_id, 
	    member_login.member_key
    FROM
	    member_race
	INNER JOIN
	    member_login
	ON 
		member_race.member_id = member_login.member_id
    WHERE
	    member_race.race_id = 2 AND
	    member_login.member_key = :Apikey AND
        member_race.role_id = 1");
        $sql->bindValue(':Apikey', $member_key);
        $sql->execute();
        $row = $sql->fetch();

        if ($row == null) {

            echo "bad api key";

        } else {

            $sql = $db->prepare("INSERT INTO
            phpclass.member_race(member_id, race_id, role_id) 
            VALUES
            (:Memberid, :Raceid, 3)");

            $sql->bindValue(':Memberid', $member_id);
            $sql->bindValue(':Raceid', $race_id);
            $sql->execute();

            echo "Runner with ID $member_id added to the race with ID $race_id!";

        }
    } catch (PDOException $e) {
        $error = $e->getMessage();
        $errors['error'] = $error;

        echo json_encode($errors);
    }
}

function uptRunner($runner_id){

    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);

    echo $post_json['test'];

}

function delRunner($runner_id){

    $request = \Slim\Slim::getInstance()->request();
    $post_json = json_decode($request->getBody(), TRUE);

    $member_id = $post_json['member_id'];
    $race_id = $post_json['race_id'];
    $member_key = $post_json['member_key'];

    //echo "$member_id => $race_id => $member_key";

    try {
        $db_dsn = 'mysql:host=localhost;dbname=phpclass';
        $db_username = "dbuser";
        $db_password = "dbdev123";
        $db_options = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,);

        $db = new PDO($db_dsn, $db_username, $db_password, $db_options);
        $sql = $db->prepare("
	SELECT
	    member_race.race_id, 
	    member_login.member_key
    FROM
	    member_race
	INNER JOIN
	    member_login
	ON 
		member_race.member_id = member_login.member_id
    WHERE
	    member_race.race_id = 2 AND
	    member_login.member_key = :Apikey AND
        member_race.role_id = 1");

        $sql->bindValue(':Apikey', $member_key);
        $sql->execute();
        $row = $sql->fetch();

        $sql->bindValue(':Apikey', $member_key);
        $sql->execute();
        $row = $sql->fetch();

        if ($row == null) {

            echo "bad api key";

        } else {

            $sql = $db->prepare("DELETE FROM
            phpclass.member_race
            WHERE
            member_id = $member_id AND 
            race_id = $race_id AND
            role_id = 3");

            $sql->bindValue(':Memberid', $member_id);
            $sql->bindValue(':Raceid', $race_id);
            $sql->execute();

            echo "Runner with ID $member_id deleted from the race $race_id!";

        }
    } catch (PDOException $e) {
        $error = $e->getMessage();
        $errors['error'] = $error;

        echo json_encode($errors);
    }

}