<?php
session_start();

$answer = 'Ask me a Question';
$question = '';

if (isset($_POST['txt_question'])) {
    $question = $_POST['txt_question'];
}

$previous_question = '';

if(isset($_SESSION['previous_question'])){
    $previous_question = $_SESSION['previous_question'];
    $_SESSION['previous_question'] = $question;
}

$responses = [
        'Ask again later…', // = 0
        'Yes', // = 1
        'No', // = 2
        'It appears to be so', // = 3
        'Reply is hazy, please try again', // = 4
        'Yes, definitely', // = 5
        'What is it you really want to know?', // = 6
        'Outlook is good', // = 7
        'My sources say no', // = 8
        'Signs point to yes', // = 9
        'Don’t count on it', // = 10
        'Cannot predict now', // = 11
        'As I see it, yes', // = 12
        'Better not tell you now', // = 13
        'Concentrate and ask again' // = 14
];

if (empty($question)){
    $answer = 'Please ask a question';
} elseif (substr($question, -1) !== '?'){
    $answer = 'Is that a statement or a question? Please use a question mark at the end of your question.';
} elseif ($previous_question == $question){
    $answer = 'Please, ask me a different question.';
} else{
    $answer = $responses[mt_rand(0,14)];
    $_SESSION['previous_question'] = $question;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="../CSS/base.css" />
    <title>Magic 8-ball</title>
</head>

<body>

<header><?php include '../includes/header.php' ?></header>

<nav><?php include '../includes/nav.php' ?></nav>

<main>
    <h2>Magic 8-Ball</h2>

    <p>
        <marquee><?=$answer?></marquee>
            <form method="post" action="8ball.php">
            <p>
                <label for="txt_question">What do you wish to know?</label>
                <br />
                <input type="text" name="txt_question" id="txt_question" value="<?=$question?>" />
            </p>

            <input type="submit" value="Answer me" />
            </form>

    </p>

</main>

<footer> <?php include '../includes/footer.php' ?></footer>

</body>

</html>