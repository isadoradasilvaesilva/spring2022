<?php
class Car{
    /**
     *The color of the vehicle
     * @var string
     */
    public $color;

    /**
     *i.e. Chevy, Dodge, Ford, Toyota
     * @var string
     */
    public $make;

    /**
     *i.e. Corvette, Viper, Mustang, Supra
     * @var string
     */
    public $model;

    /**
     *i.e. 2021, 2022
     * @var int
     */
    public $year;

    /**
     * i.e. parked, forward, reverse
     * @var string
     */
    public $status;

    /**
     * Car constructor
     */
    function __construct()
    {
        $this->status = 'parked';
    }

    /**
     * put the car in drive
     */
    function drive(){
        echo  "The car is moving forward.<br/><br/>";
        $this->status = 'drive';
    }

    /**
     * put the car in reverse
     */
    function reverse(){
        echo "The car is moving backwards.<br/><br/>";
        $this->status = 'reverse';
    }

    /**
     * put the car in parked
     */
    function park(){
        echo "The car is now parked.<br/><br/>";
        $this->status = 'parked';
    }

    /**
     * Set the color of the car
     *
     * @param string $color the color of the car (red, blue, etc.)
     * @return void
     */
    function setColor($color){
        $valid_colors = ['red','blue','green','black','white'];
        if (!in_array(strtolower($color), $valid_colors)){
            echo "Color is invalid. Try a different one! <br/><br/>";
        } else {
            $this->color = $color;
        }
    }

    /**
     * @return string
     */
    function getColor(){
        return $this->color;
    }

}// end of the class


$my_first_car = new Car;
$my_first_car->setColor('Green');
$my_first_car->make = "Dodge";
$my_first_car->model = "Shadow";
$my_first_car->year = 1994;

echo "My first car was  $my_first_car->year $my_first_car->model $my_first_car->make <br/><br/>";

$my_first_car->drive();
$my_first_car->reverse();
$my_first_car->park();




?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- <link rel="stylesheet" type="text/css" href="../CSS/base.css?ver=2.0" /> -->
    <title>Isadora's Homepage</title>
</head>

<body>

<header></header>

<nav></nav>

<main>
</main>


</body>

</html>