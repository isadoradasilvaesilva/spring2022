<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Model {

	public function user_login($user_email, $user_password){

        $this->load->database();
        $this->load->library("session");

        try{
            $db = new PDO (
                $this->db->dsn,
                $this->db->username,
                $this->db->password,
                $this->db->options
            );
            $sql = $db->prepare("
        SELECT
            password, role_id, member_key
        FROM
            phpclass.member_login
        WHERE
            email = :Email");

            $sql->bindValue(':Email', $user_email);
            $sql->execute();

            $row = $sql->fetch();
            // print_r($row); exit;
            if($row !== false){
                $hashed_password = md5($user_password.$row['member_key']);

                if($hashed_password == $row['password']){
                    //$_SESSION['UID'] = $row['member_key'];
                    //$_SESSION['ROLE'] = $row['role_id'];
                   //header("Location: Member.php");
                    $this->session->set_userdata(['UID' => $row['member_key']]);

                    return true;

                } else{
                    // $error = 'Email ou password invalid';
                    return false;
                }

            }else{
                // invalid e-mail address
                return false;
            }
        }catch (PDOException $e){
            //echo $e->getMessage();
            return false;
        }
}

    public function add_user($fullname,$email,$password){

        $this->load->database();
        $this->load->library("session");

        try{
            $db = new PDO (
                $this->db->dsn,
                $this->db->username,
                $this->db->password,
                $this->db->options
            );

            $member_key = sprintf('%04X%04X%04X%04X%04X%04X%04X%04X',
                mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
                mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535),
                mt_rand(0, 65535), mt_rand(0, 65535));

            $sql = $db->prepare(" INSERT INTO phpclass.member_login(name, email, role_id, password, member_key) 
            VALUE (:Name, :Email, :Roleid, :Password, :Key)");

            $sql->bindValue(':Name',$fullname);
            $sql->bindValue(':Email',$email);
            $sql->bindValue(':Roleid',3);
            $sql->bindValue(':Password',md5($password.$member_key));
            $sql->bindValue(':Key', $member_key);

            $sql->execute();

            return true;

        }catch (PDOException $e){
            //echo $e->getMessage();
            //exit;
            return false;
        }

    }

}



