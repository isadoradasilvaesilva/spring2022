<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->helper('form');

        $this->load->view('public/home');
	}

    public function login(){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('user_name', 'User Name', 'required|trim|valid_email');
        $this->form_validation->set_rules('password','Password','trim|required');
        // echo $this->form_validation->run(); exit();

        if($this->form_validation->run() == false){
            $data = ['load_error' => true];
            $this->load->view('public/home', $data);
        } else{
            // -- database stuff
            $this->load->model('Member');
            if($this->Member->user_login($this->input->post('user_name'),$this->input->post('password'))){

                $this->load->view('admin/home');

            }else{
                // -- bad password
                $data = ['load_error' => true, 'error_message' => 'Invalid username and/or password'];
                $this->load->view('public/home', $data);
            }

        }

    }

    public function create(){

        $this->load->library('form_validation');

        $this->form_validation->set_rules('fullname','Full Name','required');
        $this->form_validation->set_rules('username', 'User Name', 'required|trim|valid_email');
        $this->form_validation->set_rules('password','Password','trim|required');
        $this->form_validation->set_rules('password_confirm','Password Confirm','trim|required|matches[password]');
        // echo $this->form_validation->run(); exit();

        if($this->form_validation->run() == false){
            $data = ['load_error' => true];
            $this->load->view('public/home', $data);
        } else{
            // -- database stuff
            $this->load->model('Member');

            if($this->Member->add_user($this->input->post('fullname'),$this->input->post('username'),$this->input->post('password'))){

                $data = ['load_error' => true, 'error_message' => 'User was successfully created.'];
                $this->load->view('public/home', $data);

            }else{
                $data = ['load_error' => true, 'error_message' => 'Username (email) already taken.'];
                $this->load->view('public/home', $data);
            }

        }
    }
}
